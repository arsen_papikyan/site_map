import {mapGetters} from "vuex";

export default {
    ...mapGetters([ 'mapLayers', 'mapRegions', 'subjects'])
}