import {mapMutations} from "vuex";
import map from "../../services/MapService";
import axios from "axios";

export default {
    ...mapMutations(['SET_LAYER_INSTANCE', 'SET_REGIONS_INSTANCE']),

    /**
     *
     * @param layer
     * @returns {*}
     */
    loadLayer(layer) {
        this.$store.state.subjects = []

        for (let key in layer.data) {
            let layerValue = layer['data'][key]['value']
            let layerRegionKey = layer['data'][key]['region_key']
            let self = this

            this.mapRegions.features.filter(
                (data, keyRegion) => {
                    if (data['properties']['region_key'] === layerRegionKey) {
                        self.mapRegions.features[keyRegion]['properties']['value'] = layerValue
                        self.subjects.push(`${layerValue} ${data['properties']['name']}`)
                    }
                }
            )
        }

        this.updateMap(layer)

        return true
    },

    /**
     *
     * @param layer
     * @returns {*}
     */
    updateMap(layer) {
        map.setMapRegions(this.mapRegions)
        map.removeLegend()
        map.removeChoroplethLayer()

        map.initChoroplethLayer(layer.options)
        map.initLegend()

        return map.mapInstance
    },

    /**
     *
     * @returns {MapService}
     */
    createMapInstance() {
        map.initMap(this.$refs.mapContainer, [61.698883, 99.504494], this.mapRegions)

        axios.get('./layers.json')
            .then((response) => {
                    this.loadLayer(response.data[0])
                }
            )

        return map
    },

    renderMap() {
        this.$store.dispatch('receiveRegions')
        this.$store.dispatch('receiveLayers')

        this.createMapInstance()
    },


    beforeDestroy() {
        if (this.mapMutations) {
            this.mapMutations.remove()
        }
    },
}