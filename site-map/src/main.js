import { createApp } from 'vue'
import App from './App.vue'
import 'leaflet/dist/leaflet.css';
import 'bootstrap/dist/css/bootstrap.css';
import './assets/css/style.css';
import store from './store'

createApp(App).use(store).mount('#app')
