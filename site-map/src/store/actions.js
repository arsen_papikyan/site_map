import axios from "axios";

export default {
    /**
     *
     * @param commit
     * @returns {Promise<void>}
     */
    async receiveRegions({commit}) {
        const {data} = await axios.get('./regions.json')

        console.log('actions - receiveRegions ', data)

        commit('SET_REGIONS_INSTANCE', data)
    },

    /**
     *
     * @param commit
     * @returns {Promise<void>}
     */
    async receiveLayers({commit}) {
        const {data} = await axios.get('./layers.json')
        console.log('actions - receiveLayers ', data)

        commit('SET_LAYER_INSTANCE', data)
    },
}