import {createStore, createLogger} from 'vuex'
import mutations from './mutations'
import getters from './getters'
import actions from './actions'
import regions from '../assets/regions'


export default createStore({
    plugins: [createLogger()],
    state() {
        return {
            mapLayers: [],
            subjects: [],
            mapRegions: regions,
        }
    },
    mutations: mutations,
    getters:getters,
    actions: actions,
    modules: {}
})
