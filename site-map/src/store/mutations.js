export default {
    /**
     *
     * @param state
     * @param mapLayers
     * @returns {*}
     * @constructor
     */
    SET_LAYER_INSTANCE(state, mapLayers) {
        return state.mapLayers = mapLayers
    },

    /**
     *
     * @param state
     * @param mapRegions
     * @returns {*}
     * @constructor
     */
    SET_REGIONS_INSTANCE(state, mapRegions) {
        return state.mapRegions = mapRegions
    }
}