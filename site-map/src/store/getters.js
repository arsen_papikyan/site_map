export default {
    /**
     *
     * @param state
     * @returns {{}}
     */
    mapLayers(state) {
        return state.mapLayers
    },

    /**
     *
     * @param state
     * @returns {{type: string, features: {}}}
     */
    mapRegions(state) {
        return state.mapRegions
    },

    subjects(state) {
        return state.subjects
    },
}