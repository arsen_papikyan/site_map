require('leaflet-choropleth')

import L from "leaflet";
import 'leaflet/dist/leaflet.css'

let instance = null;

class MapService {

    choroplethLayer = {}

    legend = {}

    /**
     * @constructor
     */
    constructor() {
        if (!instance) {

            /**
             * Объект карты Leaflet
             * @type {LeafletMap}
             */
            this._mapInstance = null;

            /**
             *  Легенда слоя
             * @type {Object}
             */
            this.legend = null;

            /**
             *  Функция генерации легенды слоя
             * @type {Function}
             */
            this._legendFactory = null;

            this.choroplethLayer = null;
            this.mapRegions = null;


            instance = this;
        }

        return instance;
    }

    initMap(mapSelector, coordinates, mapRegions, zoom = 3) {
        if (this.mapInstance) {
            return false;
        }

        this.mapRegions = mapRegions
        this.mapInstance = L.map(mapSelector).setView(coordinates, zoom);

        return this.mapInstance;
    }

    /**
     *
     * @param options
     * @returns {*}
     */
    initChoroplethLayer(options = {}) {
        let defaultOptions =
            {
                valueProperty: 'value',
                scale: ['red', 'green'],
                steps: 5,
                mode: 'q',
                style: {color: '#fff', weight: 2, fillOpacity: 0.8},
                onEachFeature: function (feature, layer) {
                    layer.bindPopup(`District ${feature.properties.region_key} <br /> ${feature.properties.name}`)
                }
            }

        if (Object.keys(options).length !== 0) {
            defaultOptions.scale = options.scale
            defaultOptions.mode = options.mode
            defaultOptions.steps = options.steps
            defaultOptions.style = options.style
        }

        this.choroplethLayer = L.choropleth(this.mapRegions, defaultOptions).addTo(this.mapInstance)

        return this.choroplethLayer
    }

    initLegend(position = 'bottomleft') {
        this.legend = L.control({position: position})
        this.legend.onAdd = () => this.printLegend()

        return this.legend.addTo(this.mapInstance)
    }

    /**
     *
     * @returns {any}
     */
    printLegend() {
        let choroplethLayer = this.initChoroplethLayer()

        let div = L.DomUtil.create('div', 'info legend')
        let limits = choroplethLayer.options.limits
        let colors = choroplethLayer.options.colors
        let labels = []

        limits.forEach(function (limit, index) {
            labels.push(`<li style="background-color: ${colors[index]}"></li> ${limit} ${limits[index + 1] ? '- ' + limits[index + 1] : '+'} <br>`)
        })

        div.innerHTML = `${div.innerHTML}<ul> ${labels.join('')} </ul>`
        return div
    }

    setMapRegions(mapRegions) {
        if (this.mapRegions) {
            this.mapRegions = null
        }


        this.mapRegions = mapRegions
    }

    removeLegend() {
        if (this.legend) {
            this.legend.remove()
        }

        if (this.mapInstance.legend) {
            this.mapInstance.legend.remove()
        }
    }

    removeChoroplethLayer() {

        if (this.choroplethLayer) {
            this.choroplethLayer.remove()
        }

        if (this.mapInstance.layers) {
            this.mapInstance.layers.remove()
        }
    }

    setLegend(legend) {
        if (legend) {
            this.removeLegend()
            this.legend = legend;
            this.legend.addTo(this.mapInstance);
        }
    }
}


let map = new MapService()

export default map